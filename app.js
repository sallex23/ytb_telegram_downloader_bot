const fs = require('fs');
const ytdl = require('ytdl-core');
const Telegraf = require('telegraf');
const bot = new Telegraf("577679660:AAEgs_UEFPtCQVw5u2EVhQpAFUy_K9z7tSs");

bot.start((ctx) => {
    console.log('started:', ctx.from.id);
    return ctx.reply('Отправьте ссылку на видео в ютюбе чтобы скачать видео!');
})
bot.command('help', (ctx) => ctx.reply('Отправьте ссылку на видео в ютюбе'));


bot.on('message', (ctx) => {
    console.log("Сообщение от " + ctx.from.id);
    ytdl.getInfo(ctx.message.text, (err, info) => {
        if (err) {
            ctx.reply('Введена неверная ссылка либо видео не найдено');
            console.log(err);
            return;
        }
        var list = info.formats;
        list.forEach(function (element) {
            if (element.container != 'webm' && element.clen) {
                var size = formatSizeUnits(element.clen);
                var resolution = element.resolution ? element.resolution : "";
                var out = resolution == "" ? element.type : resolution;
                ctx.reply('<a href=\"' + element.url + '\">' + out + "_" + size + '</a>', { parse_mode: "HTML" });
                console.log(out + "_" + size);
            }
        });
    })
})
bot.startPolling();

//var videoID = "https://www.youtube.com/watch?v=Sxfrbm5s22U";
//ytdl.getInfo(videoID, (err, info) => {
//    if (err) throw err;
//    var list = info.formats;
//    list.forEach(function (element) {
//        if (element.container != 'webm' && element.clen) {
//            var size = formatSizeUnits(element.clen);
//            var resolution = element.resolution ? element.resolution : "";
//            var out = resolution == "" ? element.type : resolution;
//            console.log(out + "_" + size);
//        }
//    });
//})


function formatSizeUnits(bytes) {
    if (bytes >= 1073741824) { bytes = (bytes / 1073741824).toFixed(2) + ' GB'; }
    else if (bytes >= 1048576) { bytes = (bytes / 1048576).toFixed(2) + ' MB'; }
    else if (bytes >= 1024) { bytes = (bytes / 1024).toFixed(2) + ' KB'; }
    else if (bytes > 1) { bytes = bytes + ' bytes'; }
    else if (bytes == 1) { bytes = bytes + ' byte'; }
    else { bytes = '0 byte'; }
    return bytes;
}