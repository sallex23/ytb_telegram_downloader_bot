const Telegraf = require('telegraf');
const Composer = require('telegraf/composer');
const session = require('telegraf/session');
const Stage = require('telegraf/stage');
const Markup = require('telegraf/markup');
const WizardScene = require('telegraf/scenes/wizard');
const fs = require('fs');
const ytdl = require('ytdl-core');
const bot = new Telegraf("499763039:AAHXuVMbeUnAFjKKcHVbZ74Myc6L51uZ4gg");
global.stageType = 0;

const stepHandler2 = new Composer();
stepHandler2.action('youtube', (ctx) => {
    ctx.reply('Input youtube video link');
    global.stageType = 1;
    return ctx.wizard.next();
})
stepHandler2.command('youtube', (ctx) => {
    ctx.reply('Input youtube video link');
    global.stageType = 1;
    return ctx.wizard.next();
})
stepHandler2.action('vk', (ctx) => {
    ctx.reply('Insert music name');
    global.stageType = 2;
    console.log(global.stageType);
    return ctx.wizard.next();
})
stepHandler2.command('vk', (ctx) => {
    ctx.reply('Insert music name');
    global.stageType = 2;
    return ctx.wizard.next();
})
stepHandler2.use((ctx) => ctx.replyWithMarkdown('Choose button or enter (/leave) to exit'));


const superWizard = new WizardScene('super-wizard',
    (ctx) => {
        ctx.reply('Choose service',
            Markup.inlineKeyboard([
                Markup.callbackButton('Youtube link', 'youtube'),
                Markup.callbackButton('Vk', 'vk'),
            ]).extra());
        return ctx.wizard.next();
    },
    stepHandler2,
    (ctx) => {
        console.log(global.stageType);
        if (global.stageType == 2) {
            var stageType = 0;
            ctx.reply('In development');
            return;
        }
        if (global.stageType == 1) {
            console.log("from " + ctx.from.id);
            ytdl.getInfo(ctx.message.text,
                (err, info) => {
                    if (err) {
                        ctx.reply('Wrong link or cant find video');
                        console.log(err);
                    } else {
                        var list = info.formats;
                        list.forEach(function(element) {
                            if (element.container != 'webm' && element.clen) {
                                var size = formatSizeUnits(element.clen);
                                var resolution = element.resolution ? element.resolution : "";
                                var out = resolution == "" ? element.type : resolution;
                                ctx.reply('<a href=\"' + element.url + '\">' + out + "_" + size + '</a>',
                                    { parse_mode: "HTML" });
                                console.log(out + "_" + size);
                            }
                        });
                        return ctx.scene.leave();
                    }
                })
        }

    }
)

const stage = new Stage([superWizard], { default: 'super-wizard' });
bot.use(session());
bot.use(stage.middleware());
bot.startPolling();